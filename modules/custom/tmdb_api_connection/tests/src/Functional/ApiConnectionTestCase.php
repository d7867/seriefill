<?php

namespace Drupal\Tests\tmdb_api_connection\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test basic functionality of TMDB API Connection.
 *
 * @group tmdb_api_connection
 */
class ApiConnectionTestCase extends BrowserTestBase {
    /**
     * {@inheritdoc}
     */
    public static $modules = [
        'tmdb_api_connection',
    ];

    /**
     * {@inheritdoc}
     */
    protected function setUp() {
        parent::setUp();
    }
}