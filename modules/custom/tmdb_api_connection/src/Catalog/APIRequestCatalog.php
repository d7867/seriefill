<?php

namespace Drupal\tmdb_api_connection\Catalog;

use Drupal\tmdb_api_connection\Classes\APIRequester;

class APIRequestCatalog {

    /**
     * The TMBD's API key.
     *
     * @var string
     */
    private $api_key = '';

    /**
     * The API requester class.
     *
     * @var \Drupal\tmdb_api_connection\Classes\APIRequester
     */
    private $api_requester = NULL;

    public function __construct() {
        $config = \Drupal::service('config.factory')->getEditable('tmdb_api_connection.settings');
        $this->api_key = $config->get('api_key');
        $this->api_requester = new APIRequester();
    }

    /**
     * Returns the latest TV show added to the platform.
     */
    public function getTVLatestShow() {
        $endpoint = '/tv/latest';
        return $this->api_requester->get($endpoint, $this->api_key);
    }

    /**
     * Returns the top rated movies.
     *
     * @param int $page
     *   The page of the results.
     */
    public function getTopRatedMovies($page = NULL) {
        $params = [];
        if($page != NULL) {
            $params = [
                'page' => $page,
            ];
        }
        return $this->api_requester->get('/movie/top_rated', $this->api_key, $params);
    }

    /**
     * Returns detailed information of a single movie.
     *
     * @param integer $movie_id
     *   TMDB's movie ID.
     */
    public function getMovieDetails(int $movie_id) {
        $endpoint = '/movie/' . $movie_id;
        return $this->api_requester->get($endpoint, $this->api_key);
    }
}