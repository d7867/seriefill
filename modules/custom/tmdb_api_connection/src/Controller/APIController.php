<?php

namespace Drupal\tmdb_api_connection\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tmdb_api_connection\Catalog\APIRequestCatalog;

class APIController extends ControllerBase {

    /**
     * Testing purposes method
     */
    public function testing() {
        $catalog = new APIRequestCatalog();
        dump($catalog->getTopRatedMovies(1)); exit;
    }
}