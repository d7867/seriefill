<?php
/**
 * @file
 * Contains \Drupal\tmdb_api_connection\Form\APIKeyLoadForm.
 */
namespace Drupal\tmdb_api_connection\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class APIKeyLoadForm extends FormBase{
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'tmdb_api_key_load';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['api_key'] = [
            '#type' => 'textfield',
            '#title' => t('TMDB API key'),
            '#required' => TRUE,
        ];
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        if (strlen($form_state->getValue('api_key')) == 0) {
            $form_state->setErrorByName('api_key', $this->t('Please enter a valid API Key'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $config = \Drupal::service('config.factory')->getEditable('tmdb_api_connection.settings');
        $config->set('api_key', $form_state->getValue('api_key'))->save();
        \Drupal::messenger()->addStatus('Your API key was saved succesfully.');
    }
}