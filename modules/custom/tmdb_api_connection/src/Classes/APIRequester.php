<?php

namespace Drupal\tmdb_api_connection\Classes;

class APIRequester {
    /**
     * The API Base URL for requests.
     *
     * @var string
     */
    protected $api_base_url = '';

    /**
     * The language code for the results.
     *
     * @var string
     */
    protected $lang_code = '';

    public function __construct() {
        $this->api_base_url = 'https://api.themoviedb.org/3';
        $this->lang_code = 'en-US';
    }

    /**
     * Returns a formatted result of the cURL operation.
     *
     * @param string $url
     *   The URL to be fetched.
     */
    protected function curlGET(string $url) {
        $ch_session = curl_init();
        curl_setopt($ch_session, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_session, CURLOPT_URL, $url);
        return json_decode(curl_exec($ch_session));
    }

    /**
     * Get the information with query string parameters.
     *
     * @param array $params
     *   The parameters array after the '?'.
     * @param string $url
     *   The endpoint URL.
     * @param string $api_key
     *   The API key.
     */
    public function get(string $url, string $api_key, $params = NULL) {
        if ($params == NULL) {
            $params = [];
        }
        $params['api_key'] = $api_key;
        $full_url = $this->api_base_url . $url . '?' . http_build_query($params);
        return $this->curlGET($full_url);
    }
}